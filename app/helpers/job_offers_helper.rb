# Helper methods defined here can be accessed in any controller or view in the application

JobVacancy::App.helpers do
  def job_offer_params
    params[:job_offer].to_h.symbolize_keys
  end

  def show_error_messages(job_offer)
    messages = job_offer.errors.messages.map { |_key, value| value }
    messages = messages.join(', ')
    messages
  end
end
