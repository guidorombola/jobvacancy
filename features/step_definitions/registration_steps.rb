Given('I am not registered') do
  # nothing to do
end

When('I register with email {string} and password {string}, name and phone') do |mail, password|
  phone = '5491144558899'
  name = 'John'
  visit '/register'
  fill_in('user[email]', with: mail)
  fill_in('user[name]', with: name)
  fill_in('user[password]', with: password)
  fill_in('user[password_confirmation]', with: password)
  fill_in('user[phone]', with: phone)
  click_button('Create')
end

Then('I should be able to login with email {string} and password {string}') do |mail, password|
  visit '/login'
  fill_in('user[email]', with: mail)
  fill_in('user[password]', with: password)
  click_button('Login')
  page.should have_content(mail)
end
