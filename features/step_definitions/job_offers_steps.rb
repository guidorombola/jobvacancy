When(/^I browse the default page$/) do
  visit '/'
end

Given(/^I am logged in as job offerer$/) do
  visit '/login'
  fill_in('user[email]', with: 'offerer@test.com')
  fill_in('user[password]', with: 'Passw0rd!')
  click_button('Login')
  page.should have_content('offerer@test.com')
end

Given(/^I access the new offer page$/) do
  visit '/job_offers/new'
  page.should have_content('Title')
end

When(/^I fill the title with "(.*?)"$/) do |offer_title|
  fill_in('job_offer[title]', with: offer_title)
end

When(/^confirm the new offer$/) do
  click_button('Create')
end

Then(/^I should see "(.*?)" in My Offers$/) do |content|
  visit '/job_offers/my'
  page.should have_content(content)
end

Then(/^I should not see "(.*?)" in My Offers$/) do |content|
  visit '/job_offers/my'
  page.should_not have_content(content)
end

Given(/^I have "(.*?)" offer in My Offers$/) do |offer_title|
  JobOfferRepository.new.delete_all

  visit '/job_offers/new'
  fill_in('job_offer[title]', with: offer_title)
  click_button('Create')
end

Given(/^I edit it$/) do
  click_link('Edit')
end

And(/^I delete it$/) do
  click_button('Delete')
end

Given(/^I set title to "(.*?)"$/) do |new_title|
  fill_in('job_offer[title]', with: new_title)
end

Given(/^I save the modification$/) do
  click_button('Save')
end

When('I fill the required experience with {int}') do |required_experience|
  fill_in('job_offer[required_experience]', with: required_experience)
end

When('I fill the required experience with {float}') do |required_experience|
  fill_in('job_offer[required_experience]', with: required_experience)
end

Then('I should see {string} in required experience for {string} in My Offers') do |required_experience, job_title|
  visit '/job_offers/my'
  required_experience_index = 4
  xpath_expression = "//table//tr[td[contains(text(),\'#{job_title}\')]]
                        //td[#{required_experience_index}]"
  cell_value = page.find(:xpath, xpath_expression)
  expect(cell_value.text).to eq required_experience
end

Then('I should see a message in the new offer page indicating invalid required experience') do
  page.should have_content('Required experience must be a non-negative integer number')
end
