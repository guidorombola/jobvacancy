Given('only a {string} offer with experience required of {int} exists in the offers list') do |job_title, required_experience|
  @job_offer = JobOffer.new
  @job_offer.owner = UserRepository.new.first
  @job_offer.title = job_title
  @job_offer.location = 'a nice job'
  @job_offer.description = 'a nice job'
  @job_offer.required_experience = required_experience
  @job_offer.is_active = true

  JobOfferRepository.new.save @job_offer
end

When('I access the offer list page') do
  visit '/job_offers'
end

Then('I should see {string} in required experience for {string}') do |required_experience, job_title|
  required_experience_index = 4
  xpath_expression = "//table//tr[td[contains(text(),\'#{job_title}\')]]
                        //td[#{required_experience_index}]"
  cell_value = page.find(:xpath, xpath_expression)
  expect(cell_value.text).to eq required_experience
end
