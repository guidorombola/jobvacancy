Feature: Job Offers Listing
  In order to get a job
  As a candidate
  I want to see job offers

Scenario: See an offer with required experience
    Given only a 'Web Programmer' offer with experience required of 2 exists in the offers list
    When I access the offer list page
    Then I should see '2' in required experience for 'Web Programmer'

Scenario: See an offer with no required experience
    Given only a 'Web Programmer' offer with experience required of 0 exists in the offers list
    When I access the offer list page
    Then I should see 'Not specified' in required experience for 'Web Programmer'
