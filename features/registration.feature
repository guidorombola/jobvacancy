Feature: Registration
    In order to publish offers
    I want to register in Job Vacancy

Scenario: Successful Registration
    Given I am not registered
    When I register with email 'of@test.com' and password 'Passw0rd!', name and phone
    Then I should be able to login with email 'of@test.com' and password 'Passw0rd!'
