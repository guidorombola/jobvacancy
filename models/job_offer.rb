DEFAULT_REQUIRED_EXPERIENCE = 0

class JobOffer
  include ActiveModel::Validations

  attr_accessor :id, :user, :user_id, :title,
                :location, :description, :required_experience, :is_active,
                :updated_on, :created_on

  validates_presence_of :title, message: 'Title is mandatory'
  validates_numericality_of :required_experience, only_integer: true, greater_than_or_equal_to: 0,
                                                  message: 'Required experience must be a
                                                  non-negative integer number'

  def initialize(data = {})
    @id = data[:id]
    @title = data[:title]
    @location = data[:location]
    @description = data[:description]
    @required_experience = data[:required_experience] || DEFAULT_REQUIRED_EXPERIENCE
    @is_active = data[:is_active]
    @updated_on = data[:updated_on]
    @created_on = data[:created_on]
    @user_id = data[:user_id]
  end

  def owner
    user
  end

  def owner=(a_user)
    self.user = a_user
  end

  def activate
    self.is_active = true
  end

  def deactivate
    self.is_active = false
  end

  def old_offer?
    (Date.today - updated_on) >= 30
  end
end
